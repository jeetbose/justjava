package com.example.android.justjava;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This app displays an order form to order coffee.
 */
public class MainActivity extends AppCompatActivity {


    int quantity = 2;
    final int PRICE_EACH_CUP = 5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * This method is called when the order button is clicked.
     */
    public void submitOrder(View view) {
        // String priceMessage = "Total: $" + quantity * 5 + "\n" + "Thank you!";
        // displayMessage(priceMessage);
        // displayPrice(quantity*5);
        // Log.v("MainActivity",String.valueOf(cream_checkbox.isChecked()));

        EditText name_text = (EditText) findViewById(R.id.name_text_view);
        CheckBox cream_checkbox = (CheckBox) findViewById(R.id.whipped_cream_checkbox);
        CheckBox chocolate_checkbox = (CheckBox) findViewById(R.id.chocolate_checkbox);

        final String SUBJECT_LINE_TEXT = getResources().getString(R.string.order_summary_email_subject, name_text.getText());

        int price = calculatePrice( cream_checkbox.isChecked(), chocolate_checkbox.isChecked());
        // displayMessage(createOrderSummary(price, name_text.getText().toString(), cream_checkbox.isChecked(), chocolate_checkbox.isChecked()));

        String message = createOrderSummary(price, name_text.getText().toString(), cream_checkbox.isChecked(), chocolate_checkbox.isChecked());


        Intent intent = new Intent(Intent.ACTION_SEND);
        // intent.setData(Uri.parse("geo:47.6, -122.3"));
        intent.setData(Uri.parse("mailto"));
        intent.putExtra(Intent.EXTRA_SUBJECT, SUBJECT_LINE_TEXT);
        intent.putExtra(Intent.EXTRA_TEXT, message);
        intent.setType("*/*");   // message/rfc822

        if(intent.resolveActivity(getPackageManager()) != null){
            Log.v("MainActivity", "Sharing the order summary to email");
            startActivity(intent);

        }

//        try {
//            final PackageManager packageManager = this.getPackageManager();
//            List<ResolveInfo> list = packageManager.queryIntentActivities(intent, 0);
//            if(list.size() == 0) {
//                Toast.makeText(MainActivity.this, "Uh...No email app?", Toast.LENGTH_SHORT).show();
//                return;
//            }
//        }
//        catch (android.content.ActivityNotFoundException ex) {
//            Log.d("MainActivity", "No email app installed!");
//        }

    }

    /**
     * This method displays the given quantity value on the screen.
     */
    private void display(int number) {
        TextView quantityTextView = (TextView) findViewById(R.id.quantity_text_view);
        quantityTextView.setText("" + number);
    }


    /**
     *
     * @param isWhippedCreamChecked  boolean if cream checkbox is checked
     * @param isChcolocateChecked boolean if chocolate checkbox is checked
     * @return price total
     */
    public int calculatePrice(boolean isWhippedCreamChecked, boolean isChcolocateChecked) {

        int priceEachCup = PRICE_EACH_CUP + (isWhippedCreamChecked == true ? 1 : 0) + (isChcolocateChecked == true ? 2 : 0);
        return quantity * priceEachCup;
    }

    /**
     *
     * creates the order summary
     * @param price
     * @param name name of the user entered
     * @param isWhippedCreamChecked  boolean if cream checkbox is checked
     * @param isChcolocateChecked  boolean if chocolate checkbox is checked
     * @return orderSummary text to display
     */
    private String createOrderSummary(int price, String name, boolean isWhippedCreamChecked, boolean isChcolocateChecked) {

        String message =   "Name: " + name + "\n";
        message +=  "Add whipped cream? " + String.valueOf(isWhippedCreamChecked).toUpperCase() + "\n";
        message +=  "Add chocolate? " + String.valueOf(isChcolocateChecked).toUpperCase() + "\n";
        message +=  "Quantity: " + quantity + "\n";
        message +=  "Total: $" + price + "\n";
        message +=  "Thank you!";
        return message;
    }

    /**
     * This method displays the given text on the screen.
     */
//    private void displayMessage(String message) {
//        TextView orderSummaryTextView = (TextView) findViewById(R.id.order_summary_text_view);
//        orderSummaryTextView.setText(message);
//    }

    /**
     * This method is called when the plus button is clicked.
     */
    public void increment(View view) {
        if(quantity < 100)
            quantity++;
        else {
            Toast.makeText(this, "You cannot have more than 100 coffees", Toast.LENGTH_SHORT).show();
            return;
        }
        display(quantity);
    }


    /**
     * This method is called when the minus button is clicked.
     */
    public void decrement(View view) {
        if(quantity > 1)
            quantity--;
        else {
            Toast.makeText(this, "You cannot order less than 1 coffee", Toast.LENGTH_SHORT).show();
            return;
        }

        display(quantity);

    }

}